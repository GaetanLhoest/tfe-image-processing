im = imread('matlab1.jpg');
imw = size(im,2);
imh = size(im,1);

R = im(:,:,1);
G = im(:,:,2);
B = im(:,:,3);

sqsize = 5;

counter = 1;

for i = sqsize+1:1:(imh-sqsize)
    for j = sqsize+1:1:(imw-sqsize)

        guardian = 1;        
        
        r = R(i-sqsize:i+sqsize,j-sqsize:j+sqsize);
        g = G(i-sqsize:i+sqsize,j-sqsize:j+sqsize);
        b = B(i-sqsize:i+sqsize,j-sqsize:j+sqsize);

        
            
        
        r = r(:);
        g = g(:);
        b = b(:);
        for k = 1:1:(((sqsize*2)+1)^2)
            if r(k) == 255 & g(k) == 255 & b(k) == 255
                guardian = 0;
            end
        end

        if guardian == 1
            m(counter,1) = mean(r);
            m(counter,2) = mean(g);
            m(counter,3) = mean(b);

            vr(counter,1) = var(double(r));
            vr(counter,2) = var(double(g));
            vr(counter,3) = var(double(b));

            stdev(counter,1) = std(double(r));
            stdev(counter,2) = std(double(g));
            stdev(counter,3) = std(double(b));

            counter = counter + 1;
            if mod(counter, 1000) == 0
                disp(counter);
            end
        end
        
    end
end

