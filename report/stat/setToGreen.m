function  setToGreen(ij, im)
    im = imread(im);
    
    for i = 1:1:size(ij,1)
        for j = ij(i,1)-5:1:ij(i,1)+5
            for k = ij(i,2)-5:1:ij(i,2)+5
                im(j,k,1) = 0;
                im(j,k,2) = 255;
                im(j,k,3) = 0;
            end
        end
    end
    image(im)
    
end