function [m, vr, stdev, ij] = squarestatsgray(name, sqsize, nbr)
    im = imread(name);
    im = rgb2gray(im);
    
    imw = size(im,2);
    imh = size(im,1);
    
    w = imw - (2*sqsize);
    h = imh - (2*sqsize);
    
    diff = w*h;
    tmpdiff = w*h;
    stepi = 1;
    guardian = 0;
    
    while guardian == 0
        guardian = 1;
        tmpdiff = abs(nbr - ((w*h)/(stepi*stepi)));
        if tmpdiff < diff
            diff = tmpdiff;
            guardian = 0;
            stepi = stepi + 1;
        end
    end
    
    if nbr > (w*h)
        nbr = w*h;
    end
    
    
    m = zeros(size(sqsize+1:stepi:(imh-sqsize),1),1);
    vr = zeros(size(sqsize+1:stepi:(imh-sqsize),1),1);
    stdev = zeros(size(sqsize+1:stepi:(imh-sqsize),1),1);
	ij = zeros(size(sqsize+1:stepi:(imh-sqsize),1),1);
    counter = 1;
    for i = sqsize+1:stepi:(imh-sqsize)
        for j = sqsize+1:stepi:(imw-sqsize)

            s = im(i-sqsize:i+sqsize,j-sqsize:j+sqsize);
            
            s = s(:);
            
            m(counter) = mean(s);
            
            vr(counter) = var(double(s));
            
            stdev(counter) = std(double(s));
            
            ij(counter,1) = i;
            ij(counter,2) = j;
            
            counter = counter + 1;
        end
    end
end
