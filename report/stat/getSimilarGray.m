function nij = getSimilarGray(ij, m, vr)

    counter = 1;
    nij = zeros(1,2);
    for i = 1:1:size(ij,1)
        if ((m(i) > 124) && (m(i) < 130) && (vr(i) > 177) && (vr(i) < 184))
            nij(counter,:) = ij(i,:);
            counter = counter + 1;
        end
    end

end