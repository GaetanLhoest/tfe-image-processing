im = imread('/home/gaetan/matlab1.jpg');
im = rgb2gray(im);

imw = size(im,2);
imh = size(im,1);

sqsize = 5;

counter = 1;

for i = sqsize+1:1:(imh-sqsize)
    for j = sqsize+1:1:(imw-sqsize)

        guardian = 1;        
        
        s = im(i-sqsize:i+sqsize,j-sqsize:j+sqsize);
      
       
        s = s(:);
        
        for k = 1:1:(((sqsize*2)+1)^2)
            if s(k) == 255
                guardian = 0;
            end
        end

        if guardian == 1
            m(counter) = mean(s);

            vr(counter) = var(double(s));

            stdev(counter) = std(double(s));

            counter = counter + 1;
            
            if mod(counter, 1000) == 0
                disp(counter);
            end
        end
        
    end
end

