function [m, vr, stdev, ij] = squarestats(name, sqsize, nbr)
    im = imread(name);
    R = im(:,:,1);
    G = im(:,:,2);
    B = im(:,:,3);
    
    imw = size(im,2);
    imh = size(im,1);
    
    w = imw - (2*sqsize);
    h = imh - (2*sqsize);
    
    diff = w*h;
    tmpdiff = w*h;
    stepi = 1;
    guardian = 0;
    
    while guardian == 0
        guardian = 1;
        tmpdiff = abs(nbr - ((w*h)/(stepi*stepi)));
        if tmpdiff < diff
            diff = tmpdiff;
            guardian = 0;
            stepi = stepi + 1;
        end
    end
    
    if nbr > (w*h)
        nbr = w*h;
    end
    
    
    m = zeros(size(sqsize+1:stepi:(imh-sqsize),1),3);
    vr = zeros(size(sqsize+1:stepi:(imh-sqsize),1),3);
    stdev = zeros(size(sqsize+1:stepi:(imh-sqsize),1),3);
    ij = zeros(size(sqsize+1:stepi:(imh-sqsize),1),2);
    counter = 1;
    for i = sqsize+1:stepi:(imh-sqsize)
        for j = sqsize+1:stepi:(imw-sqsize)

            r = R(i-sqsize:i+sqsize,j-sqsize:j+sqsize);
            g = G(i-sqsize:i+sqsize,j-sqsize:j+sqsize);
            b = B(i-sqsize:i+sqsize,j-sqsize:j+sqsize);
            
            r = r(:);
            g = g(:);
            b = b(:);
            
            m(counter,1) = mean(r);
            m(counter,2) = mean(g);
            m(counter,3) = mean(b);
            
            vr(counter,1) = var(double(r));
            vr(counter,2) = var(double(g));
            vr(counter,3) = var(double(b));
            
            stdev(counter,1) = std(double(r));
            stdev(counter,2) = std(double(g));
            stdev(counter,3) = std(double(b));
            
            ij(counter,1) = i;
            ij(counter,2) = j;
            
            counter = counter + 1;
        end
    end
end
